Rails.application.routes.draw do

  devise_for :consultants
  devise_for :workers
  devise_for :admins

  root 'pages#index'
  get '/admin-page', to: 'pages#admin'
  get '/worker-page', to: 'pages#worker'
  get '/consultant-page', to: 'pages#consultant'
  get '/admin-phone-list', to: 'pages#adminphones'
  get '/admin-worker-list', to: 'pages#adminworkers'
  get '/admin-consultant-list', to: 'pages#adminconsultants'

  devise_for :users
  resources :phones
end
