class PagesController < ApplicationController
before_action :authenticate_admin!, only: [:admin, :adminphones, :adminworkers, :adminconsultants]
before_action :authenticate_worker!, only: [:worker]
before_action :authenticate_consultant!, only: [:consultant]

  def index
    @phones = Phone.all
  end

  def admin
    sign_out :user
    sign_out :worker
    sign_out :consultant
  end

  def worker
    sign_out :admin
    sign_out :user
    sign_out :consultant
  end

  def consultant
    sign_out :admin
    sign_out :user
    sign_out :worker
  end

  def adminphones
    @phones = Phone.all
  end

  def adminworkers
    @workers = current_admin.workers.all
  end

  def adminconsultants
    @cons = current_admin.consultants.all
  end
end
