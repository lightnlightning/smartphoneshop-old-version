class AddAdminIdToWorkers < ActiveRecord::Migration[5.1]
  def change
    add_column :workers, :admin_id, :integer
  end
end
